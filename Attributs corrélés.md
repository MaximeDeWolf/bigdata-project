# Attributs corrélés

Liste des attributs fortement corrélés.

|           Attributs corrélés            |  Corrélation  |
| :-------------------------------------: | :-----------: |
|        pdays - poutcome.success         | -0.9812010016 |
|       previous - poutcome.failure       | 0.9365154439  |
|     previous - poutcome.nonexistent     | -0.9780846678 |
|    marital.married - marital.single     | -0.7571203563 |
|      default.no - default.unknown       | -0.999735429  |
|        housing.no - housing.yes         | -0.9528634252 |
|     housing.unknown - loan.unknown      |       1       |
|           loan.no - loan.yes            |  -0.9155996   |
|  contact.cellular - contact.telephone   |      -1       |
| poutcome.failure - poutcome.nonexistent | -0.9614240443 |

