library(tidyverse)
library(caret)

kTraining <- 1
kValidation <- 2
kTest <- 3

OneHotEncode <- function(data){
  # Apply the one-hot encoding on data.
  # 'data' is a data frame. Be carerul that it contains only categorical predictors.
  
  dummy.vars <- dummyVars(" ~ .", data = data)
  newdata <- data.frame(predict(dummy.vars, newdata = data))
  
  return(newdata)
}

CategoricalToNumeric <- function(data, predictors = NULL){
  # Convert the (categorial) predictors to numeric predictors using the one-hot encoding.
  # 'data' is a data frame and 'predictors' is a vector of predictor names.
  # If 'predictors' is set to NULL, then all data will be converted to numeric.
  
  if (is.null(predictors)){
    return(OneHotEncode(data))
  }
  
  # Split data to categorical data and numeric data.
  categorical.data <- dplyr::select(data, predictors)
  numeric.data <- dplyr::select(data, -predictors)
  
  # Convert categorical data to numeric data.
  data <- cbind(numeric.data, OneHotEncode(categorical.data))
  
  return(data)
}

RemoveCorrelatedPredictors <- function(data, threshold = 0.99){
  # Remove correlated predictors, that is for two correlated predictors (|correlation| > 'threshold'),
  # one of these is remove (randomly chosen).
  # 'data' is a data frame and 'threshold' is a real between 0 and 1.
  
  # Compute the positive correlations.
  correlations <- abs(cor(data, use = "complete.obs"))
  
  # Search correlated predictors to remove.
  predictors.to.remove <- list()
  predictors <- names(data)
  pred.indexes <- seq_along(predictors)
  for (i in pred.indexes){
    for (j in pred.indexes){
      if (i < j & correlations[i, j] >= threshold){
        if (!(i %in% predictors.to.remove) & !(j %in% predictors.to.remove)){
          pred.to.remove.index <- sample(c(i, j), 1)
          pred.to.remove <- predictors[pred.to.remove.index]
          predictors.to.remove <- append(predictors.to.remove, pred.to.remove)
        }
      }
    }
  }
  
  # Remove duplicates predictors in the list.
  predictors.to.remove <- unlist(predictors.to.remove)
  
  # Remove the predictors.
  if (length(predictors.to.remove) > 0){
    data <- dplyr::select(data, -predictors.to.remove)
  }
  
  return(data)
}

RemoveObsMissVal <- function(data, predictors, threshold = 0.04){
  # Remove observations with missing values by looking only at the 'predictors' predictors with a threshold.
  # In fact, for each predictor, removal is only performed when less than 'threshold * 100'% of data are missing
  # (otherwise nothing is done). So, 'length(predictors) * threshold' data can be removed.
  # 'data' is a data frame and 'predictors' is a vector of predictor names.
  
  num.rows <- nrow(data)
  
  observations.toremove <- list()
  i <- 1
  for (predictor in predictors){
    na.indexes <- which(is.na(data[, predictor]))
    
    num.na <- length(unknown.indexes)
    na.rate <- num.unknowns / num.rows
    if (na.rate < threshold){
      # Only a maximum of 'threshold'% data will be removed.
      observations.toremove[[i]] <- unknown.indexes
      i <- i + 1
    }
  }
  
  # Remove duplicates from 'observations.toremove'.
  observations.toremove <- unique(unlist(observations.toremove))
  
  # Remove observations.
  data <- data[-observations.toremove,]
  
  # Drop levels.
  for (predictor in predictors){
    data[, predictor] <- droplevels(data[, predictor])
  }
  
  return(data)
}

ReplaceNAByMeanFromBinPred <- function(data, predictors) {
  # Handle the missing values from binary predictors (each one contains following values: 0, 1 and NA).
  # For each predictor, the mean of non missing values is computed and the missing values are replaced by this mean.
  # 'data' is a data frame and 'predictors' is a vector of predictor names.
  
  # Copy the data frame.
  data <- data.frame(data)
  
  for (predictor in predictors){
    preddata <- data[, predictor]
    predmean <- mean(preddata, na.rm = TRUE)
    data[, predictor][is.na(preddata)] <- predmean
  }
  
  return(data)
}

ScaleData <- function(data, except = NULL){
  # Scale (normalize) all predictors from data except the 'expect' predictors.
  # 'data' is a data frame and 'except' is a vecotr of predictor names.
  
  if (is.null(except)){
    scaled.data <- data.frame(scale(data))
  } else{
    # Split data.
    data.toscale <- dplyr::select(data, -except)
    data.tonotscale <- dplyr::select(data, except)
  
    # Scale data.
    data.toscale <- data.frame(scale(data.toscale))
    scaled.data <- cbind(data.tonotscale, data.toscale)
  }
  
  # When the variance of a predictor is 0, 'scale' function returns NaN.
  # However, it should be 0. This is fixed below.
  num.rows <- nrow(scaled.data)
  for (predictor in names(scaled.data)){
    preddata <- scaled.data[, predictor]
    if (all(is.nan(preddata))){
      scaled.data[, predictor] <- rep(0, num.rows)
    }
  }
  
  return(scaled.data)
}

PCA <- function(data){
  # Apply PCA on a data frame (only works on numeric data).
  pca <- prcomp(data, center = TRUE, scale. = TRUE)
  
  return(pca)
}

PropagatePCA <- function(data, pca){
  # Create new parameters in the dataset according to the PCA values.
  new.data <- predict(pca, newdata = data)
  
  return(new.data)
}

PreprocessNum <- function(data){
  # Preprocess data (not during the cross-validation). The resulting data is numeric.
  # 'data' is a data frame.

  # Replace "unknown" values by NA.
  data[data == "unknown"] <- NA
  data <- droplevels(data) # Remove the "unknown" label in the factor (header).
  
  # Convert categorical data to numeric data.
  # Non binary predictors.
  categorical.predictors = c("job", "marital", "contact", "month", "day_of_week", "poutcome", "edu")
  data <- CategoricalToNumeric(data, predictors = categorical.predictors)
  # For original predictors with only two values, remove one new created predictor.
  #data <- dplyr::select(data, -contact.telephone) # Two possible values: "cellular" and "telephone".
  # Binary predictors (convert 'yes' to 1 and 'no' to 0).
  translate <- c(yes = 1, no = 0)
  data$default <- translate[as.character(data$default)]
  data$housing <- translate[as.character(data$housing)]
  data$loan <- translate[as.character(data$loan)]
  
  return(data)
}

PreprocessNumTrainTest <- function(data, set.type){
  # Preprocess data during the training and the test (and also the cross-validation). The resulting data is numeric.
  # 'data' is a data frame and 'set.type' must be to 'kTraining' if 'data' is a training, 'kValidation'
  # if it is a validation set, 'kTest' if it is the test set.
  
  if (set.type == kTraining){
    # If 'data' is a training set.
    remove.correlated.predictors <- TRUE
    allow.removing.observations <- TRUE
    predictors.tonotscale <- NULL
  } else if(set.type == kValidation){
    # If 'data' is a training set.
    remove.correlated.predictors <- FALSE
    allow.removing.observations <- FALSE
    predictors.tonotscale <- NULL
  } else if(set.type == kTest){
    # If 'data' is a test set.
    remove.correlated.predictors <- FALSE
    allow.removing.observations <- FALSE
    predictors.tonotscale <- c("id")
  } else{
    stop("'set.type' must be set to kTraining, kValidation or kTest.")
  }
  
  # Remove correlated predictors.
  if (remove.correlated.predictors == TRUE){
    data <- RemoveCorrelatedPredictors(data)
  }
  
  # Handle missing values.
  # Remove some observations with missing values.
  if (allow.removing.observations == TRUE){
    predictors <- c("job", "martial", "edu")
    
    #data <- RemoveObsMissVal(data, predictors)
  }
  # For binary predictors, replace the missing values by the mean.
  pred.names <- names(data)
  numeric.predictors <- c("age", "campaign", "pdays", "previous", "poutcome", "y")
  binary.predictors <- pred.names[!(pred.names %in% numeric.predictors)]
  data <- ReplaceNAByMeanFromBinPred(data, binary.predictors)
  
  # Scale data.
  data <- ScaleData(data, except = predictors.tonotscale)
  
  return(data)
}

PreprocessCat <- function(data){
  # Preprocess data (not during the cross-validation). The resulting data is categorical.
  # 'data' is a data frame.
  
  # Convert numeric data to categorical data.
  data$age <- findInterval(data$age, seq(0, 100, by = 10))
  data$campaign <- findInterval(data$campaign, seq(1, 60, by = 5))
  data$pdays <- findInterval(data$pdays, c(0, 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 998))
  
  # Convert categorical data to numeric data.
  categorical.predictors <- c("job", "marital", "default", "housing", "loan", "contact", "month", "day_of_week", "poutcome", "edu")
  for (predictor in categorical.predictors){
    data[, predictor] <- as.integer(unclass(data[, predictor]))
  }
  data <- droplevels(data) # Remove the labels in the factors (headers).
  
  return(data)
}

PreprocessCatTrainTest <- function(data, set.type){
  # Preprocess data during the training and the test (and also the cross-validation). The resulting data is categorical
  # 'data' is a data frame and 'set.type' must be to 'kTraining' if 'data' is a training, 'kValidation'
  # if it is a validation set, 'kTest' if it is the test set.
  
  return(data)
}
